import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerThread2 extends Thread{
	Socket socket;
	int i;
	BufferedReader input;
	public ServerThread2(Socket socket){
		this.socket = socket;
	}
	public void run(){
		try{
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			while(true){
				String echoString = input.readLine();
				String id[];
				id = echoString.split(":");
				if("exit".equalsIgnoreCase(echoString)){
					break;
				}
				// thread1.transferMessage(echoString);
				// if(i == 0){
					MainWindow.map.get(id[id.length-1]).transferMessage(echoString);
					// System.out.println("Current: " + i);
				// }else{
					// ServerCode.arrayList.get(0).transferMessage(echoString);
					// System.out.println("Current: " + i);
				// }
				
				// System.out.println("Client Sent: "+ echoString);
			}
		}catch(Exception e){
				System.out.println("Server Exception: thread2"+e);
                                e.printStackTrace();
		}finally{
			try{
				socket.close();
			}catch(IOException e){

			}
		}
	}
}