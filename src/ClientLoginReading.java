
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sahil
 */
public class ClientLoginReading extends Thread{
    Socket socket;
    String response="";
    ClientThread1 thread1;
    Login2 loginWindow;
    public ClientLoginReading(Socket socket,Login2 loginWindow){
        this.socket = socket;
        this.loginWindow = loginWindow;
    }
    public void run(){
        try{
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while(true){
                    // if(input.readLine() != null){
                    // System.out.println("In while!!!!!!");
                    System.out.println("inside client reading");
                    response = input.readLine();
                     System.out.println(response);
                    if("SUCCESSFUL".equals(response)){
                        String allUsers[] = response.split(":");
                        loginWindow.dispose();
                        AllUsernames as = new AllUsernames(allUsers);
                        as.setVisible(true);
                        for(int i=0;i<allUsers.length;i++)
                        System.out.println(allUsers[i]);
                        
                    }else if("UNSUCCESSFUL".equals(response)){
                        System.out.println(response);
                        JOptionPane.showMessageDialog(null,"Wrong Username or Password");
                    }else if("ALREADY_LOGGED_IN".equals(response)){
                        JOptionPane.showMessageDialog(null,"You are logged from another device");
                    }
                    // }
            }
        }catch(IOException e){
            System.out.println("Client Exception 2" + e.getMessage());
        }
//        finally{
//            try{
//                    socket.close();
//            }catch(IOException e){
//
//            }
//        }
    }
    
}
