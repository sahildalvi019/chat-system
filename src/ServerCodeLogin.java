import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerCodeLogin{
//	static int i=0;
	public static void main(String[] args) {
		try(ServerSocket serverSocket = new ServerSocket(7001)){
			while(true){
				Socket socket = serverSocket.accept();
				System.out.println("Client Connected in server Login");
				ServerLoginReading thread2 = new ServerLoginReading(socket);
				thread2.start();
			}
		}catch(IOException e){
			System.out.println("Server Exception in main: " +e);
		}
	}
}