import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;

public class ServerLoginReading extends Thread{
	Socket socket;
	int i;
	BufferedReader input;
	Connection conn;
	PreparedStatement ps = null;
	ResultSet rs = null;
        String userName;
	public ServerLoginReading(Socket socket){
		this.socket = socket;
		conn = MySQLConnect.getConnection();
	}
        ArrayList<String> isAvailable = new ArrayList<>();
	public void run(){
		try{
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			while(true){
                                System.out.println("inside server login reading");
				String echoString = input.readLine();
				String id[] = new String[2];
				id = echoString.split(":");
				String userName = id[0];
				String password = id[1];
                                this.userName = userName;
                                System.out.println(userName);
                                System.out.println(password);
				String sql = "SELECT * FROM user WHERE username = ? AND password = ?";
				ps = conn.prepareStatement(sql);
				ps.setString(1,userName);
				ps.setString(2,password);

				rs = ps.executeQuery();

				if(rs.next()){
                                    if(isAvailable.contains(userName)){
                                        ServerLoginWriting slw = new ServerLoginWriting(socket,"ALREADY_LOGGED_IN",conn);
                                        slw.start();
                                    }else{
                                        isAvailable.add(userName);
                                        ServerLoginWriting slw = new ServerLoginWriting(socket,"SUCCESSFUL",conn);
                                        slw.start();
                                    }
				}else{
                                    ServerLoginWriting slw = new ServerLoginWriting(socket,"UNSUCCESSFUL",conn);
                                    slw.start();
				}

			}
		}catch(Exception e){
				System.out.println("Server Exception: thread2"+e);
                                e.printStackTrace();
		}finally{
			try{
				socket.close();
			}catch(IOException e){

			}
		}
	}
}